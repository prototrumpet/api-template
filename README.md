# Template API

Fork this API template and replace the database Terraform info, API paths, resource tenant names, etc. with your own stuff.

## Dev Instructions
- nvm i
- yarn
- yarn lint:fix
- yarn format
- Make deploy
