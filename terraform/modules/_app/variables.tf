variable "lambda_bucket_name" {
  type = string
  description = "AWS S3 Bucket Name for storing built lambda zip files"
}

variable "lambda_code_zip" {
  type = string
  description = "Reference to your AWS Lambda code zip file. Recommend using path.cwd var to build a relative path to your build directory"
}

variable "handler_function" {
  type = string
  description = "Name of your handler function so AWS can invoke your lambda"
}

variable "runtime" {
  type = string
  description = "AWS Function runtime environment"
}

variable "timeout" {
  type = number
  description = "Function timeout"
  default = 28
}

variable "memory_size" {
  type = number
  description = "Memory to allocate for function in MB"
  default = 2048
}

variable "env_vars" {
  type = map(string)
  description = "Map of environment variables to apply to your function"
}

variable "service_domain_name" {
  type = string
  description = "FQDN for your service domain name, ex: myapp.domain.com"
}

variable "certificate_arn" {
  type = string
  description = "The ARN for your AWS ACM Certificate to use for validating domain"
}

variable "zone_id" {
  type = string
  description = "The Route 53 Hosted Zone Id for this endpoint"
}

variable "region" {
  type = string
  description = "The AWS Region"
}

variable "app_description" {
  type = string
  description = "Description of your app"
  default = "A serverless application"
}

variable "iam_permission_boundary_arn" {
  type = string
  description = "ARN for Permission Boundary"
}

variable "app_name" {
  type = string
  description = "The name of the application"
}

variable "resource_prefix" {
  type = string
  description = "The resource prefix, should be tenant-app"
}
