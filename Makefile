ENVIRONMENT = "test"

deploy: build
	$(MAKE) -C terraform/env/$(ENVIRONMENT)

build: install
	yarn package

install:
	yarn
