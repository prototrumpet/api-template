import { NextFunction, Request, Response } from 'express';
import { container } from '../container';
import { Logger } from 'winston';

const logger: Logger = container.resolve('Logger');

export function loggerMiddleware(req: Request, res: Response, next: NextFunction): void {
  logger.info(`${req.method} ${req.path}`);
  next();
}
