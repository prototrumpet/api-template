import { UserEntity } from './UserEntity';

export class User {
  constructor(
    public id: string,
    public lastUpdated: number
  ) {}

  public toEntity(): UserEntity {
    return {
      id: this.id,
      lastUpdated: this.lastUpdated,
    };
  }

  public static fromEntity(entity: UserEntity): User {
    return new User(entity.id, entity.lastUpdated);
  }
}
