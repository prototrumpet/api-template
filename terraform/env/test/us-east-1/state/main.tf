terraform {
  required_version = ">= 0.13"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 3.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

locals {
  tenant = "cliffdevs"
  application = "apitemplate"
  resource_prefix = "${local.tenant}-${local.application}"
}

module "state_management" {
  source = "../../../../modules/terraform_aws_state_backend"

  resource_prefix = local.resource_prefix
}
