variable "resource_prefix" {
  type = string
  description = "The prefix for resources"
}

variable "current_region" {
  type = string
  description = "The current region the provider is deploying"
}

variable "execution_iam_role_id" {
  type = string
  description = "The IAM role id to grant access to the dynamodb"
}
