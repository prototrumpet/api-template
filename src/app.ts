import 'source-map-support/register';
import 'reflect-metadata';
import express from 'express';
import bodyParser from 'body-parser';
import * as AWSXRay from 'aws-xray-sdk';
import usersRouter from './routes/users';
import { loggerMiddleware } from './middleware/logger-middleware';
import { errorHandlerMiddleware } from './middleware/error-handler-middleware';

const app = express();

// Enable X-Ray tracing before your routes are defined
// eslint-disable-next-line @typescript-eslint/no-var-requires
AWSXRay.captureHTTPsGlobal(require('http'));
app.use(AWSXRay.express.openSegment('users'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded());
app.use(loggerMiddleware);
app.use(errorHandlerMiddleware);
app.use('/api/users', usersRouter);

export default app;
