data "aws_caller_identity" "current" {}

locals {
  table_name = "${var.resource_prefix}-users"
}

resource "aws_dynamodb_table" "app_dynamodb" {
  count = var.current_region == "us-east-1" ? 1 : 0

  name = local.table_name

  billing_mode = "PAY_PER_REQUEST"
  hash_key = "id"

  attribute {
    name = "id"
    type = "S"
  }

  stream_enabled = true
  stream_view_type = "NEW_AND_OLD_IMAGES"

  replica {
    region_name = "us-west-2"
  }
}

resource "aws_iam_role_policy" "app_dynamodb_access_policy" {
  name = "${var.resource_prefix}-${local.table_name}-${var.current_region}-policy"
  role = var.execution_iam_role_id

  policy = templatefile("${path.module}/dynamodb_access_policy.json", {
    table_name = local.table_name,
    region = var.current_region
  })
}
