output "api_uri" {
  value = "https://${module.serverless_application.api_host}/${local.base_path}"
}
