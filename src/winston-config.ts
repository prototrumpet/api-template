import { createLogger, format, transports } from 'winston';

const { combine, timestamp, printf } = format;

const myFormat = printf(({ level, message, logTimestamp }) => {
  return `${logTimestamp} ${level}: ${message}`;
});

export const logger = createLogger({
  level: 'info',
  format: combine(timestamp(), myFormat),
  transports: [new transports.Console()],
});
