import express from 'express';
import { container } from '../container';
import { UserService } from '../service/UserService';

const router = express.Router();
const userService = container.resolve(UserService);

// Get a specific profile
router.get('/:userId', async (req, res) => {
  const userId = req.params.userId;
  try {
    const user = await userService.getProfile(userId);
    res.json(user);
  } catch (error) {
    const err = error as Error;
    if (err.message === 'Not Found') {
      res.sendStatus(404);
    } else {
      throw error;
    }
  }
});

// Save a profile
router.put('/:userId', async (req, res) => {
  const profileData = req.body;
  await userService.saveProfile(profileData);
  res.sendStatus(200);
});

router.delete('/:userId', async (req, res) => {
  const userId = req.params.userId;
  await userService.deleteProfile(userId);
  res.sendStatus(200);
});

export default router;
