locals {
  base_path = "/api/users"
}

module "serverless_application" {
  source = "../serverless_application"

  app_name = var.app_name
  app_description = "Serverless API Template"
  certificate_arn = var.certificate_arn
  env_vars = merge(var.env_vars, {
    TABLE_NAME = module.application_dynamodb.table_name,
  })
  handler_function = var.handler_function
  lambda_bucket_name = var.lambda_bucket_name
  lambda_code_zip = var.lambda_code_zip
  region = var.region
  runtime = var.runtime
  service_domain_name = var.service_domain_name
  zone_id = var.zone_id
  iam_permission_boundary_arn = var.iam_permission_boundary_arn
  health_check_id = null
  api_key_required = false
  resource_prefix = var.resource_prefix
}

module "application_dynamodb" {
  source = "../_app_dynamodb"

  current_region = var.region
  execution_iam_role_id = module.serverless_application.execution_role_id
  resource_prefix = var.resource_prefix
}
