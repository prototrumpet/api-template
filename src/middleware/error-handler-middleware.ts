import { NextFunction, Request, Response } from 'express';
import { logger } from '../winston-config';

export function errorHandlerMiddleware(
  err: Error,
  req: Request,
  res: Response,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  next: NextFunction
): void {
  logger.error('Unhandled error:', err);

  res.status(500).json({
    error: 'Internal Server Error',
    message: err.message, // You might choose to omit this in production to avoid exposing internal details
  });
}
