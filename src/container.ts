import { container } from 'tsyringe';
import { DynamoDbRepo } from './data/DynamoDbRepo';
import { logger } from './winston-config';

container.register('DynamoDbRepo', { useClass: DynamoDbRepo });
container.register('Logger', { useValue: logger });

export { container };
