import { inject, injectable } from 'tsyringe';
import { DynamoDbRepo } from '../data/DynamoDbRepo';
import { User } from '../data/User';

@injectable()
export class UserService {
  constructor(@inject('DynamoDbRepo') private dynamoDbRepo: DynamoDbRepo) {}

  public async saveProfile(user: User): Promise<void> {
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore
    await this.dynamoDbRepo.saveUser(new User(user.id, new Date().getUTCDate()));
  }

  public async getProfile(id: string): Promise<User> {
    return this.dynamoDbRepo.getUser(id);
  }

  public async deleteProfile(id: string): Promise<void> {
    await this.dynamoDbRepo.deleteProfileMetadata(id);
  }
}
