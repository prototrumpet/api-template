import { inject, injectable } from 'tsyringe';
import AWS from 'aws-sdk';
import { User } from './User';
import { UserEntity } from './UserEntity';
import { Logger } from 'winston';

const documentClient = new AWS.DynamoDB.DocumentClient();
const tableName = process.env.TABLE_NAME as string;

@injectable()
export class DynamoDbRepo {
  constructor(@inject('Logger') private logger: Logger) {}

  public async saveUser(profileMetadataModel: User): Promise<void> {
    try {
      await documentClient
        .put({
          TableName: tableName,
          Item: profileMetadataModel.toEntity(),
        })
        .promise();
    } catch (error) {
      this.logger.error(`Unable to saveProfileMetadata: ${error}`);
      throw error;
    }
  }

  public async getUser(id: string): Promise<User> {
    try {
      const result = await documentClient
        .get({
          TableName: tableName,
          Key: {
            id,
          },
        })
        .promise();
      const entity = result.Item as UserEntity;
      if (entity) {
        return User.fromEntity(entity);
      }

      throw new Error('Not Found');
    } catch (error) {
      const err = error as Error;
      if (err.message !== 'Not Found') this.logger.error(`Unable to getProfileMetadata ${id}: ${err}`);
      throw error;
    }
  }

  public async deleteProfileMetadata(id: string): Promise<void> {
    try {
      await documentClient
        .delete({
          TableName: tableName,
          Key: {
            id,
          },
        })
        .promise();
    } catch (error) {
      this.logger.error(`Unable to deleteProfileMetadata ${id}: ${error}`);
    }
  }
}
