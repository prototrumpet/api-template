terraform {
  required_version = ">= 0.13"

  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 3.0"
    }
    archive = {
      source = "hashicorp/archive"
    }
    null = {
      source = "hashicorp/null"
    }
  }

  backend "s3" {
    bucket = "cliffdevs-apitemplate-terraform-state"
    key = "apitemplate-us-east-1"
    region = "us-east-1"

    dynamodb_table = "cliffdevs-apitemplate-terraform-locks"
    encrypt = true
  }
}

provider "aws" {
  region = "us-east-1"
}

provider "archive" {}
provider "null" {}

locals {
  current_region = "us-east-1"
  tenant = "cliffdevs"
  app = "apitemplate"
  domain = "refactur.com"

  resource_prefix = "${local.tenant}-${local.app}"
  service_domain_name = "${local.app}.${local.domain}"
}

module "application" {
  source = "../../../../modules/_app"

  certificate_arn = "arn:aws:acm:us-east-1:484855333663:certificate/3d69a3c7-a7d1-497d-bc6b-48c2956998a7"
  env_vars = {}

  handler_function = "dist/lambda.handler"
  lambda_bucket_name = "${local.resource_prefix}-${local.current_region}"
  lambda_code_zip = "${path.cwd}/../../../../../build/function.zip"
  region = local.current_region
  runtime = "nodejs18.x"
  service_domain_name = local.service_domain_name
  zone_id = "Z02108771DB4A349RNFUI"
  app_name = local.app
  resource_prefix = local.resource_prefix
  iam_permission_boundary_arn = null
}

output "api_uri" {
  value = module.application.api_uri
}