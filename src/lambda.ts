import { APIGatewayProxyHandler } from 'aws-lambda';
import serverlessExpress from 'aws-serverless-express';
import app from './app';

const server = serverlessExpress.createServer(app);

export const handler: APIGatewayProxyHandler = (event, context) => {
  return serverlessExpress.proxy(server, event, context, 'PROMISE').promise;
};
