export interface UserEntity {
  id: string;
  lastUpdated: number;
}
